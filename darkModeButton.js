document.addEventListener('DOMContentLoaded', function() {
    var darkModeOptions = {
        brightness: 150,
        contrast: 80,
        sepia: 0,
    };
    
    var button = document.createElement("Button");
    
    if (Cookies.get("darkModeToggle") === "1") {
        DarkReader.enable(darkModeOptions);
        button.innerHTML = "Light Mode";
    } else {
        button.innerHTML = "Dark Mode";
    }
    
    button.style = "top:0;right:0;position:absolute;z-index: 9999";
    
    button.addEventListener("click", function (e) {
        if (Cookies.get("darkModeToggle") === "1") {
            DarkReader.disable()
            Cookies.set("darkModeToggle", "0", { expires: 30 });
            this.innerHTML = "Dark Mode";
        } else {
            DarkReader.enable(darkModeOptions);
            Cookies.set("darkModeToggle", "1", { expires: 30 });
            this.innerHTML = "Light Mode";
        }
    });
    
    document.body.appendChild(button);
});